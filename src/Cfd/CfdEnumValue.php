<?php

namespace ElegantTechnologies\Cfd;

#use ElegantTechnologies\Cfd\Cfd_Base;

#require_once (__DIR__ . '/property_EnumValue_Validates.php');

abstract class CfdEnumValue extends Cfd_Base {
    /** @var YourTypeGoesHereWhenYouSubClassThis_IfSeeingThis_ThenCopyThisLineAndNextWithRightTtype */
    public $EnumValue;

    use property_EnumValue_Validates;
}

