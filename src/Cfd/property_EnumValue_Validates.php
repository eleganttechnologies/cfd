<?php

namespace ElegantTechnologies\Cfd;
trait property_EnumValue_Validates
{
        public static $_ArrEnumValuePossibilities;


    public static function EnumValue_Validates($maybeValidItem): \ElegantTechnologies\Cfd\DtoValueValidation {
        #return new \src\Cfd\DtoValueValidation(['isValid' => true]);
        $asr_AsrRichProperties = static::getRichProperties();
        $asrRichProperties = $asr_AsrRichProperties['properties'];

        if (!isset($asrRichProperties['_ArrEnumValuePossibilities']) || !($asrRichProperties['_ArrEnumValuePossibilities']['hasStaticDefault'])) {
            $thisClassName = get_called_class();
            $class = new \ReflectionClass($thisClassName);
            $asrStaticProperties = $class->getProperties();

            $arrStaticPropertyName = array_column($asrStaticProperties, 'name');
            $csvStaticPropertyName = implode(', ', $arrStaticPropertyName);
            $hasPropertyThere = in_array('_ArrEnumValuePossibilities', $arrStaticPropertyName);
            if (!$hasPropertyThere) {
                return new DtoValueValidation(['isValid'=>false, 'enumReason'=>'_ArrEnumValuePossibilities_notExisting','message'=>"This is an CfdEnumValue, so you must set \$_ArrEnumValuePossibilities as 'public \$_ArrEnumValuePossibilities = [];' with an array of allowed values. The current properties set in  '$thisClassName' are $csvStaticPropertyName"]);
                #throw CfdError::doesHave_NotFalseFor_AccompanyingProperty("This is an CfdEnumValue, so you must set \$_ArrEnumValuePossibilities as 'public \$_ArrEnumValuePossibilities = [];' with an array of allowed values. The current properties set in  '$thisClassName' are $csvStaticPropertyName");
            } else {
                // state: _ArrEnumValuePossibilities is set


                $hasEnumValuesSet = isset($thisClassName::$_ArrEnumValuePossibilities);
                if (!$hasEnumValuesSet) {
                    return new DtoValueValidation(['isValid' => false, 'enumReason' => '_ArrEnumValuePossibilities_notSet', 'message' => "This is an CfdEnumValue($thisClassName), so you must set \$_ArrEnumValuePossibilities to an array, like 'public \$_ArrEnumValuePossibilities = [];' with an array of allowed values. "]);
                }

                $isListOfPossibleEnumValuesActuallyAnArray = is_array($thisClassName::$_ArrEnumValuePossibilities);
                if (!$isListOfPossibleEnumValuesActuallyAnArray) {
                    $setToX =  $thisClassName::$_ArrEnumValuePossibilities;
                    $setToType = gettype($setToX);
                    return new DtoValueValidation(['isValid' => false, 'enumReason' => '_ArrEnumValuePossibilities_notSetToBeAnArray', 'message' => "This is an CfdEnumValue ($thisClassName), so you must set \$_ArrEnumValuePossibilities to an array, like 'public \$_ArrEnumValuePossibilities = [];' with an array of allowed values. It is set to '$setToX' which is of type '$setToType', but it instead needs to be set to be an array."]);
                }
            }
        }


        if (!in_array($maybeValidItem, $asrRichProperties['_ArrEnumValuePossibilities']['staticDefault'])) {
            return new \ElegantTechnologies\Cfd\DtoValueValidation(['isValid' => false, 'enumReason' => 'NotReal', 'message' => "'$maybeValidItem' is not valid enum from list : " . implode(', ', $asrRichProperties['_ArrEnumValuePossibilities']['staticDefault'])]);
        }
        return new \ElegantTechnologies\Cfd\DtoValueValidation(['isValid' => true]);
    }
}