<?php
declare(strict_types=1);
namespace ElegantTechnologies\Cfd;

//use ElegantTechnologies\Cfd\Cfd_Base;
//use ElegantTechnologies\Cfd\CfdError;

#require_once(__DIR__ . '/property_EnumValue_Validates.php');

abstract class CfdEnumValues extends Cfd_Base
{
    /** @var YourTypeGoesHereWhenYouSubClassThis_IfSeeingThis_ThenCopyThisLineAndNextWithRightTtype */
    public $EnumValues;

    use property_EnumValue_Validates;

    public static function EnumValues_Validates(array $maybeValidItems): \ElegantTechnologies\Cfd\DtoValueValidation
    {

        foreach ($maybeValidItems as $maybeValidItem) {
            $dtoValid = static::EnumValue_Validates($maybeValidItem);
            if ($dtoValid->isValid == false) {
                return $dtoValid;
            }
        }
        return new \ElegantTechnologies\Cfd\DtoValueValidation(['isValid' => true]);

    }

    public function doesHaveThis($doesHave_singleValue) : bool
    {
         $arrDoesHaves = $this->EnumValues;
        return (in_array($doesHave_singleValue, $arrDoesHaves,true));
    }

    public function doesHaveThese(array $arrMustHaves) : bool
    {
        $arrDoesHaves = $this->EnumValues;
        return (array_intersect($arrMustHaves, $arrDoesHaves) == $arrMustHaves);
        // $array1 is a subset of $array2
        //https://stackoverflow.com/a/12276627/93933
    }

     public function doesHaveOnlyThis($doesHave_singleValue) : bool
    {
         $arrDoesHaves = $this->EnumValues;
        return (count($arrDoesHaves) == 1 && $this->doesHaveThis($doesHave_singleValue));
    }
}
