<?php
// 12/19' Should be moved to Library
namespace ElegantTechnologies\Cfd;

#require_once(__DIR__ . '/Cfd_Base.php');
#use ElegantTechnologies\Cfd\DtoValueValidation;

class CfdYmd extends \ElegantTechnologies\Cfd\Cfd_Base {
    /** @var string */
    public $Ymd;

    public static function Ymd_Validates($maybeValidValue) : \ElegantTechnologies\Cfd\DtoValueValidation {
        $t = date("Y-m-d",strtotime($maybeValidValue)); // https://xkcd.com/1179/

        if ($maybeValidValue == $t) {
            return new DtoValueValidation(['isValid' => true]);
        } else {
            return new DtoValueValidation(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue" ]);
        }
    }
}

