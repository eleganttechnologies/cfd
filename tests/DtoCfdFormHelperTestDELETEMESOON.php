<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


require_once(__DIR__ . '/../Cfd_Base.php');
require_once(__DIR__ . '/../DtoCfdFormHelper.php');


trait beAPerson  {
    /** @var string|null */
    public $FirstName;
    /** @var string|null*/
    public $LastName;
    #/** @var boolean */
    #public $doesHave_FirstName;
    #/** @var boolean*/
    #public $doesHave_LastName;
}

class DtoPersonAttendee  extends \ElegantTechnologies\Cfd\Cfd_Base {
    use beAPerson;
}

class DtoPersonBooker  extends \ElegantTechnologies\Cfd\Cfd_Base  {
    use beAPerson;

    /** @var boolean */
    public $doesHave_Email;

    /** @var string */
    public $Email;

    /** @var boolean */
    public $doesHave_Phone;

    /** @var string|null */
    public $Phone;
}


final class TestDtoCfdFormHelper_GetForm_InputFields extends TestCase {
    function assertArrayValuesMatch(array $arrExpected, array $arrGot, string $ErrorPrefix = '')
    {

        $arrDiff = array_diff($arrExpected, $arrGot);
        $strDiff = implode(", ", $arrDiff);
        $strExpected = implode(", ", $arrExpected);
        $strGot = implode(", ", $arrGot);
        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so missing  $strDiff)");

        $arrDiff = array_diff($arrGot, $arrExpected);
        $strDiff = implode(", ", $arrDiff);
        $strExpected = implode(", ", $arrExpected);
        $strGot = implode(", ", $arrGot);
        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so have extras $strDiff)");
    }

    function test_GetForm_InputFields() {
        $realInputFriendlyNames = ['FirstName','LastName'];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['FirstName','LastName'];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['FirstName','LastName', 'Email', 'Phone'];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames(DtoPersonBooker::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

    }

      function test_GetForm_InputFields_ThatAreRequired() {
        $realInputFriendlyNames = [];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = [];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['Email'];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonBooker::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");



        $realInputFriendlyNames = ['Phone', 'FirstName', 'Email'];
        $extraNamesWeAreForcingToBeRequired = ['FirstName', 'Phone'];
        $foundNames = \ElegantTechnologies\Cfd\Cfd_BaseFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonBooker::class, $extraNamesWeAreForcingToBeRequired);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
    }


}