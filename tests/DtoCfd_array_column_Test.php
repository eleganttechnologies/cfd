<?php
declare(strict_types=1);
namespace testworld;

use ElegantTechnologies\Cfd\Cfd_Base;
use PHPUnit\Framework\TestCase;
#use \EtElegantTechnologes\Pages;

#require_once(__DIR__ . '/../../../vendor/autoload.php');

class DtoTabForTestingStuff extends Cfd_Base{
    /** @var string */
     public $Slug;
    /** @var string */
    public $Text;
}



final class TestDtoCfd_array_column extends TestCase {
    function testBasics() {
        $obj = new \testworld\DtoTabForTestingStuff(['Slug' => 'Delete', 'Text'=>'Trash']);
        $this->assertTrue(isset($obj), "Good");

    }

    function testMore() {
        $arrTabs = [
            new \testworld\DtoTabForTestingStuff(['Slug' => 'Delete', 'Text'=>'Trash']),
           new \testworld\DtoTabForTestingStuff(['Slug' => 'Merging', 'Text'=>'Shrink']),
            ];
        $arrSlugs = Cfd_Base::arrDto_column($arrTabs, 'Slug');
          $this->assertTrue($arrSlugs[0] == 'Delete', "ok");
          $this->assertTrue($arrSlugs[1] == 'Merging', "ok");
          $this->assertTrue(count($arrSlugs) ==2 , "ok");



    }
}
