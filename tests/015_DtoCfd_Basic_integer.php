<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


#require_once(__DIR__ . '/../../../vendor/autoload.php');


class DtoCfdInteger extends \ElegantTechnologies\Cfd\Cfd_Base
{
    /** @var integer */
    public $val;
}

class DtoCfdInteger_optional extends \ElegantTechnologies\Cfd\Cfd_Base
{
    /** @var integer|null */
    public $val;
}

class DtoCfdInteger_optionalReversedOrder extends \ElegantTechnologies\Cfd\Cfd_Base
{
    /** @var null|integer */
    public $val;
}


abstract class TestDtoCfd_BaseInt extends TestCase
{
    static $CfdName = 'TBD';
    static $ValName = 'TBD';
    static $isNullAnOption = 'TBD';

    function testPreValidationsSubmission_byProperty()
    {
        $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, 1, null);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, "1", null);
        $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

    }

    function testPreValidationsSubmission_byProperty_null()
    {
        $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, null, null);
        $this->assertTrue($dtoValueValidation->isValid == static::$isNullAnOption, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);
    }


    function testPreValidationsSubmission_easy()
    {
        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => "Hello"]);
        $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this:  " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => "1"]);
        $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this:  " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => ""]);
        $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this:  " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

    }

    function testPreValidationsSubmission_allowed()
    {
        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => 1]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => 99]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => -1]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);

        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => 0]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);
    }


    function testPreValidationsSubmission_null()
    {
        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => null]);
        $this->assertTrue($dtoValueValidation->isValid == static::$isNullAnOption, "Should not see this:  " . get_called_class() . " dtoValueValidation({$dtoValueValidation->message}). " . __LINE__);
    }
}

final class TestDtoCfd_Submission_OneVal extends TestDtoCfd_BaseInt
{
    static $CfdName = 'DtoCfdInteger';
    static $ValName = 'val';
    static $isNullAnOption = false;
}

final class TestDtoCfd_Submission_OneVal_orNull extends TestDtoCfd_BaseInt
{
    static $CfdName = 'DtoCfdInteger_optional';
    static $ValName = 'val';
    static $isNullAnOption = true;
}

final class TestDtoCfd_Submission_OneVal_orNullReordered extends TestDtoCfd_BaseInt
{
    static $CfdName = 'DtoCfdInteger_optionalReversedOrder';
    static $ValName = 'val';
    static $isNullAnOption = true;
}
