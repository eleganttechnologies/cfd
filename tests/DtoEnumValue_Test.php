<?php
declare(strict_types=1);
namespace testworld;



use ElegantTechnologies\Cfd\CfdEnumValue;
use PHPUnit\Framework\TestCase;

#require_once(__DIR__ . '/../../../vendor/autoload.php');
class DtoCfdInt extends \ElegantTechnologies\Cfd\Cfd_Base {
    /** @var int */
    public $val;
}

class CfdEnumPhaseSimple extends CfdEnumValue {
    /** @var string */
    public $EnumValue;
    public static $_ArrEnumValuePossibilities = ['Draft', 'RollOut', 'LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];

}

class CfdEnumPhase extends \ElegantTechnologies\Cfd\CfdEnumValue {
    /** @var string */
    public $EnumValue;
    public static $bob = 'hi';
    public static $_ArrEnumValuePossibilities = ['Draft','RollOut','LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];
}

class CfdEnumSmallPrimes extends CfdEnumValue {
    /** @var integer */
    public $EnumValue;
    public static $_ArrEnumValuePossibilities = [1,2,3,5,7];
}


final class TestDtoEnumPhase1 extends TestCase {

    function testBasics() {
        $obj = new CfdEnumPhaseSimple(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");

    }

        function testBasics2() {
        $obj = new CfdEnumPhase(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");


        try {
            $obj = new \testworld\CfdEnumPhase(['EnumValue' => 'Explosion']);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }

    function testDtoEnumSmallPrimes() {
        $obj = new CfdEnumSmallPrimes(['EnumValue' => 1]);
        $this->assertTrue($obj->EnumValue == 1, "Good");


        try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => 4]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }



         try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => 6]);
            $this->assertTrue(0, "Should not get this far cuz a string");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

         try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => '3']);
            $this->assertTrue(0, "Should not get this far");
         } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }
}
