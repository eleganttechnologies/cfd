<?php
declare(strict_types=1);
namespace testworld;

use PHPUnit\Framework\TestCase;
#use \EtElegantTechnologes\Pages;
//
//if (!defined('WP_PLUGIN_DIR')) {
//    define('WP_PLUGIN_DIR', __DIR__ . '/../../../plugins');
//}
//require_once(__DIR__ . '/../Cfd_Base.php');

#require_once(__DIR__ . '/../../vendor/autoload.php');

class DtoDummy extends \ElegantTechnologies\Cfd\Cfd_Base {
    /** @var integer */
    public $Even;

    public static function Even_Validates($maybeValidValue) : \ElegantTechnologies\Cfd\DtoValueValidation {
        $isEven = ($maybeValidValue % 2) == 0;
        if ($isEven) {
            return new \ElegantTechnologies\Cfd\DtoValueValidation(['isValid' => true]);
        } else {
            return new \ElegantTechnologies\Cfd\DtoValueValidation(['isValid' => false, 'enumReason'=>'HasRemainder']);
        }
    }
}


final class TestDtoCfd extends TestCase {


    function testBasics() {
        $obj = new \testworld\DtoDummy(['Even'=>0]);
        $this->assertTrue($obj->Even == 0, "Good");

        try {
            $obj = new \testworld\DtoDummy(['Even'=>1]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        $obj = new \testworld\DtoDummy(['Even'=>2]);
        $this->assertTrue($obj->Even == 2, "Good");

        $obj = new \testworld\DtoDummy(['Even'=>-4]);
        $this->assertTrue($obj->Even == -4, "Good");

        try {
            $obj = new \testworld\DtoDummy(['Even'=>-3]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }


        try {
            $obj = new \testworld\DtoDummy(['Even'=>-1]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy(['Even'=>3]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }


}