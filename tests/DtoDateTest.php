<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



#require_once(__DIR__ . '/../../../vendor/autoload.php');


final class TestDtoDate extends TestCase {


    function testMake() {
        try {
            $badDbt = ';lakjsdf;';
            $obj = new \ElegantTechnologies\Cfd\CfdYmd(['Ymd'=>$badDbt]);
            $this->assertTrue(0, "1Should not get this far ut: ".strtotime($badDbt));
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \ElegantTechnologies\Cfd\CfdYmd(['Ymd'=>0]);
            $this->assertTrue(0, "2Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "2Good - that faiiled as expected");
        }

        try {
            $obj = new \ElegantTechnologies\Cfd\CfdYmd(['Ymd'=>1970]);
            $this->assertTrue(0, "3Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "3Good - 1970 is not a string, plus it is a vague date");
        }

        try {
            $obj = new \ElegantTechnologies\Cfd\CfdYmd(['Ymd'=>'tomorrow']);
            $this->assertTrue(0, "4Should not get this far");
        } catch (\ElegantTechnologies\Cfd\CfdError $e) {
            $this->assertTrue(true, "4Good - that failed as expected");
        }


        $obj = new \ElegantTechnologies\Cfd\CfdYmd(['Ymd'=>'1970-11-04']);
        $this->assertTrue(isset($obj), "");


    }


}